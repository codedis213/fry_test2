# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class DmozItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    domain = scrapy.Field()
    itemurl = scrapy.Field()
    categoriestags = scrapy.Field()
    title = scrapy.Field()
    author = scrapy.Field()
    authorlink = scrapy.Field()
    serves = scrapy.Field()
    imglink = scrapy.Field()
    ingredients = scrapy.Field()
