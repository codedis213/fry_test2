# myapp/mongoadmin.py

# Import the MongoAdmin base class
from mongonaut.sites import MongoAdmin

# Import your custom models
from recipeapp.models import Recipe, User


class RecipeAdmin(MongoAdmin):

    def has_view_permission(self, request):
        return True

    def has_edit_permission(self, request):
        return True

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request):
        return True

    search_fields = ('title', 'author')
    list_fields = ('title', "author", "created_date", "published_dates")


class UserAdmin(MongoAdmin):
    def has_view_permission(self, request):
        return True

    def has_edit_permission(self, request):
        return True

    def has_add_permission(self, request):
        return True

    list_fields = ('first_name', "last_name", "email")


Recipe.mongoadmin = RecipeAdmin()
User.mongoadmin = UserAdmin()
