from django.db import models

# Create your models here.

# -*- coding: utf-8 -*-

"""
The main purpose of these models is to do manual testing of
the mongonaut front end.  Do not use this code as an actual blog
backend.
"""

from datetime import datetime

from mongoengine import BooleanField
from mongoengine import DateTimeField
from mongoengine import Document
from mongoengine import EmbeddedDocument
from mongoengine import EmbeddedDocumentField
from mongoengine import ListField
from mongoengine import ReferenceField
from mongoengine import StringField


class User(Document):
    email = StringField(required=True, max_length=50)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)

    def __unicode__(self):
        return self.email


class Comment(EmbeddedDocument):
    message = StringField(default="DEFAULT EMBEDDED COMMENT")
    author = ReferenceField(User)

    # ListField(EmbeddedDocumentField(ListField(Something)) is not currenlty supported.
    # UI, and lists with list inside them need to be fixed.  The extra numbers appened to
    # the end of the key and class need to happen correctly.
    # Files to fix: list_add.js, forms.py, and mixins.py need to be updated to work.
    # likes = ListField(ReferenceField(User))


class EmbeddedUser(EmbeddedDocument):
    email = StringField(max_length=50, default="default-test@test.com")
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    created_date = DateTimeField()  # Used for testing
    is_admin = BooleanField()  # Used for testing
    # embedded_user_bio = EmbeddedDocumentField(Comment)
    friends_list = ListField(ReferenceField(User))

    # Not supportted see above comment on Comment
    # user_comments = ListField(EmbeddedDocumentField(Comment))


class Recipe(Document):
    # See Post.title.max_length to make validation better!
    domain = StringField(max_length=500, required=True)
    itemurl = StringField(max_length=500, required=True)
    categoriestags = ListField()
    title = StringField(max_length=256, required=True)
    author = StringField(max_length=100, required=True)
    authorlink = StringField(max_length=500, required=True)
    serves = StringField(max_length=50, required=True)
    imglink = StringField(max_length=500, required=True)
    ingredients = StringField(max_length=1000, required=True)
    created_date = DateTimeField()
    published_dates = ListField(DateTimeField())   

    def save(self, *args, **kwargs):
        if not self.created_date:
            self.created_date = datetime.utcnow()
        if self.published:
            self.published_dates.append(datetime.utcnow())
        super(Recipe, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title

    class Meta:
        # At default, the form fields will be ordered by python sorted()
        # if form_fields_ordering is set under model's Meta class,
        # ordering will be prioritized, then remaining fields are sorted
        form_fields_ordering = ('first_name', 'last_name', 'email', )

